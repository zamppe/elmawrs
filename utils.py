import datetime
import re
import time

import arrow
import requests
from bs4 import BeautifulSoup


def get_site_contents(link) -> BeautifulSoup:
    """Just a wrapper for mundane stuff."""
    res = requests.get(link)
    res.raise_for_status()  # Raise an exception if something bad happens.
    return BeautifulSoup(res.content, 'lxml')


def time_string_to_int(s: str) -> int:
    if not (',' in s or ':' in s or '.' in s):
        return int(s)

    parts = re.split('[:,.]', s)
    parts = list(map(int, parts))
    if len(parts) == 2:
        secs, hunds = parts
        return secs * 100 + hunds

    mins, secs, hunds = parts
    return mins * 60 * 100 + secs * 100 + hunds


def time_int_to_string(t: int) -> str:
    mins = t // 6000
    secs = t // 100 % 60
    hunds = t % 100
    return f'{mins:02}:{secs:02},{hunds:02}'


def timeit(f):
    """ Timing decorator."""

    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        print(f'Took {(te - ts):.3f}s')
        return result

    return timed


def mopo_date_string_to_datetime(date_string: str) -> datetime.datetime:
    return arrow.get(date_string, 'D MMMM YYYY').naive


def db_date_to_mopo_date_string(db_date: datetime.date) -> str:
    return arrow.get(db_date).format('D MMMM YYYY')


def nationality_to_country(nat: str) -> str:
    return {
        'American': 'United States',
        'Argentinian': 'Argentina',
        'Australian': 'Australia',
        'British': 'United Kingdom',
        'Canadian': 'Canada',
        'Czech': 'Czech Republic',
        'Danish': 'Denmark',
        'Dutch': 'Netherlands',
        'Finnish': 'Finland',
        'Hungarian': 'Hungary',
        'Icelandic': 'Iceland',
        'Israeli': 'Israel',
        'Lithuanian': 'Lithuania',
        'Norwegian': 'Norway',
        'Polish': 'Poland',
        'Russian': 'Russia',
        'Slovak': 'Slovakia',
        'Swedish': 'Sweden',
    }[nat]


if __name__ == '__main__':
    print(time_int_to_string(13010))
