from pathlib import Path

from peewee import *

DB_FILENAME = 'world_records.db'
WR_DB = SqliteDatabase(Path(DB_FILENAME))


class BaseModel(Model):
    class Meta:
        database = WR_DB
        legacy_table_names = False


class Player(BaseModel):
    name = CharField(unique=True)
    country = CharField(default='')


class WRTable(BaseModel):
    id = IntegerField(primary_key=True)
    date = DateField()


class Level(BaseModel):
    id = IntegerField(primary_key=True)
    name = CharField()


class WRTime(BaseModel):
    table = ForeignKeyField(WRTable, backref='wrtimes', index=True)
    player = ForeignKeyField(Player, backref='wrtimes', index=True)
    level = ForeignKeyField(Level, backref='wrtimes', index=True)
    time = IntegerField()
    player_team = CharField()
