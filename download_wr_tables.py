import datetime
import re
import time
from dataclasses import dataclass
from typing import List, Set

from db_models import WR_DB, Player, WRTable, WRTime, Level
from utils import get_site_contents, time_string_to_int, mopo_date_string_to_datetime, timeit, nationality_to_country

LINK = 'https://moposite.com/records_elma_wrs.php'
DELAY = 0.5


@dataclass
class TableEntry:
    level: int
    time: int
    player_name: str
    player_team: str
    level_name: str


class TablePage:
    def __init__(self, id_: int = None):
        if id_ is None:
            link = LINK
        else:
            link = f'{LINK}?table={id_}'

        self.page = get_site_contents(link)

    def get_table_count(self) -> int:
        table_selector = self.page.find('select', attrs={'name': 'table'})
        return len(table_selector.find_all('option'))

    def get_date_of_update(self) -> datetime.datetime:
        updated_string = self.page.find('b', text=re.compile('Updated')).text
        return mopo_date_string_to_datetime(updated_string)

    def get_all_entries(self) -> List[TableEntry]:
        all_entries = []
        player_pattern = re.compile(r'(.+) \[(.*)\]')
        table = self.page.find('table', class_='wrtable')

        # skip table header
        entries = table.find_all('td')[6:]

        # group by 3 elements
        for level_element, time_element, player_element in zip(entries[0::3], entries[1::3], entries[2::3]):
            level, level_name = level_element.string.split('. ')
            wr_time = time_string_to_int(time_element.string)

            if len(player_element.contents) == 3:
                player_name, player_team = re.search(player_pattern, player_element.text).groups()
            elif len(player_element.contents) == 2:
                player_name = player_element.text.strip()
                player_team = player_element.contents[1]['href'][5:]
            else:
                player_name = player_element.text
                player_team = ''

            all_entries.append(TableEntry(level=int(level), time=wr_time, player_name=player_name,
                                          player_team=player_team, level_name=level_name))

        return sorted(all_entries, key=lambda x: x.level)


class WRDevelopmentPage:
    def __init__(self):
        link = 'https://moposite.com/records_wr_statistics.php?wr_development_all_submit=All+levels'
        self.page = get_site_contents(link)

    def get_all_nationalities(self) -> Set[str]:
        all_nats = set()

        for x in self.page.find_all('a', class_='noUnderline halfLink', title=re.compile('Nationality')):
            _, nat = x['title'].split(': ')
            all_nats.add(nat)

        return all_nats

    def find_player_nationality(self, player: str) -> str:
        player_link = self.page.find('a', class_='noUnderline halfLink', title=re.compile(f'Player: {player}'))
        parent_row = player_link.parent.parent
        nat_tag = parent_row.contents[2].contents[0]
        _, nat = nat_tag['title'].split(': ')
        return nat


def download_multiple_tables(table_list: List[int]):
    with WR_DB.atomic():
        WR_DB.create_tables([Player, WRTable, WRTime, Level])

        # keep a local dict so we don't need to read db for every player id
        existing_players = {x.name: x.id for x in Player.select()}

        # only add levels in db if they don't already exist
        have_levels_in_db = bool(Level.get_or_none())

        for table_id in table_list:
            if WRTable.get_or_none(id=table_id):
                print(f'Table {table_id} already exists in the database')
                continue

            print(f'Parsing table {table_id}...')
            page = TablePage(table_id)
            date = page.get_date_of_update()
            entries = page.get_all_entries()

            # add players
            players_in_table = set([x.player_name for x in entries])
            new_players_in_table = players_in_table - existing_players.keys()
            if new_players_in_table:
                print(f'Added new players: {new_players_in_table}')
                q = Player.insert_many([(x,) for x in new_players_in_table], fields=[Player.name])
                q.execute()
                existing_players = {x.name: x.id for x in Player.select()}

            # add wr table
            WRTable.create(id=table_id, date=date)

            # add levels
            if not have_levels_in_db:
                q = Level.insert_many([(x.level, x.level_name) for x in entries], fields=[Level.id, Level.name])
                q.execute()
                have_levels_in_db = True

            # add times
            entries_to_insert = [(table_id, existing_players[x.player_name],
                                  x.time, x.level, x.player_team) for x in entries]
            q = WRTime.insert_many(entries_to_insert, fields=[WRTime.table, WRTime.player,
                                                              WRTime.time, WRTime.level, WRTime.player_team])
            q.execute()

            # sleep so abula doesn't bane
            time.sleep(DELAY)


@timeit
def download_all_tables(n: int = None):
    if n is None:
        table_count = TablePage().get_table_count()
    else:
        table_count = n

    print(f'Going to download {table_count} WR tables')
    tables_to_parse = list(range(1, table_count + 1))
    download_multiple_tables(tables_to_parse)


@timeit
def update_player_countries():
    if Player.select().where(Player.country == '').count() == 0:
        print('All players have countries, canceling the update')
        return

    page = WRDevelopmentPage()

    with WR_DB.atomic():
        for player in Player.select():
            player_nat = page.find_player_nationality(player.name)
            player_country = nationality_to_country(player_nat)

            q = Player.update(country=player_country).where(Player.name == player.name)
            q.execute()


if __name__ == '__main__':
    update_player_countries()
