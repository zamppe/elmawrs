import logging
from typing import Dict

from peewee import fn, SQL, ModelSelect

import utils as ut
from db_models import Player, WRTable, WRTime

logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


def get_players() -> Dict[int, str]:
    return {x.id: x.name for x in Player.select()}


def get_total_time_for_table(table_id: int) -> int:
    return sum([x.time for x in WRTime.select(WRTime.time).where(WRTime.table_id == table_id)])


def display_wr_table(table_id: int):
    if not WRTable.get_or_none(id=table_id):
        print(f'No table #{table_id} in the database')
        return

    date = WRTable.get(WRTable.id == table_id).date

    print(f'WR Table #{table_id}')
    print(f'Updated {ut.db_date_to_mopo_date_string(date)}\n')
    for wr in WRTime.select().where(WRTime.table_id == table_id):
        full_lev_name = f'{wr.level.id:02}. {wr.level.name}'
        team = f' [{wr.player_team}]' if wr.player_team else ''
        print(f'{full_lev_name:<25}  {ut.time_int_to_string(wr.time)}  {wr.player.name}{team}')

    tt = ut.time_int_to_string(get_total_time_for_table(table_id))
    print(f'\nTotal time: {tt}')


def wr_holders_per_table() -> ModelSelect:
    return (WRTime
            .select(WRTable.id,
                    Player.name,
                    fn.SUM(WRTime.time).alias('cum_time'),
                    fn.COUNT(Player.id).alias('wr_count'))
            .join_from(WRTime, WRTable)
            .join_from(WRTime, Player)
            .group_by(WRTable.id, Player.id)
            .order_by(WRTable.id.asc(), SQL('cum_time').desc()))


def display_wr_holders_per_table():
    current_wr_table = 1
    for wr in wr_holders_per_table():
        if wr.table.id > current_wr_table:
            print('-' * 35)
            current_wr_table += 1
        print(f'{wr.table.id:03}  {wr.player.name:<12}  '
              f'{ut.time_int_to_string(wr.cum_time)}  {wr.wr_count}')


if __name__ == '__main__':
    display_wr_holders_per_table()
